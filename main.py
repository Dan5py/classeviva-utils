import os
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from dotenv import load_dotenv

from classes.ClassevivaNote import *
import utils.classeviva as classeviva
from utils.logger import CustomLogger

# Load env file
load_dotenv()


options = Options()
options.headless = True
driver = None

try:
    driver = webdriver.Firefox(options=options)
    driver.get(os.getenv("CLASSEVIVA_URL") or "")
    CustomLogger.log("Firefox (Headless) instance initialized")
except Exception as err:
    CustomLogger.error(f"Connection error: {err}")
    if driver:
        driver.close()
    exit(1)


def main():
    try:
        classeviva.login_to_classeviva(driver)
        # In "libretto web" the title is the url
        assert os.getenv("CLASSEVIVA_URL") or "" in driver.title

        note = ClassevivaNote(
            notetype=ClassevivaNoteType.USCITA,
            causal=ClassevivaNoteCausal.TRASPORTO,
            notedate="12/10/2022",
            notetime="12:20",
        )
        classeviva.new_note(driver, note=note)

    except Exception as err:
        CustomLogger.error(err)


if __name__ == "__main__":
    main()
    driver.quit()
