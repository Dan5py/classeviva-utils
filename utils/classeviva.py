import os
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from classes.ClassevivaNote import ClassevivaNote, ClassevivaNoteType
from utils.logger import CustomLogger


def login_to_classeviva(driver: webdriver.Firefox):
    login_page_title = "Login CVV Classeviva Gruppo Spaggiari Parma"

    if driver.title != login_page_title:
        raise Exception("Not in login page")

    username_field = driver.find_element(By.ID, "login")
    password_field = driver.find_element(By.ID, "password")

    if not (username_field and password_field):
        raise Exception("Login fields not found")

    username_field.clear()
    password_field.clear()
    username_field.send_keys(os.getenv("CLASSEVIVA_USER"))
    password_field.send_keys(os.getenv("CLASSEVIVA_PASSWORD"))
    password_field.send_keys(Keys.RETURN)

    # Wait for login, max 10 seconds
    WebDriverWait(driver, 10).until(
        lambda d: d.title != login_page_title, message="Login failed"
    )

    CustomLogger.info(f"Logged in as {os.getenv('CLASSEVIVA_USER')}")


def new_note(driver: webdriver.Firefox, note: ClassevivaNote):

    WebDriverWait(driver, 5).until(
        lambda d: d.find_element(By.CLASS_NAME, "nuova_giustifica"),
        message="New note button not found",
    )

    #######################
    #   OPEN A NEW NOTE   #
    #######################
    new_note_btn = driver.find_element(By.CLASS_NAME, "nuova_giustifica")
    new_note_btn.click()

    #######################
    #  SELECT NOTE TYPE   #
    #######################
    note_type_div = driver.find_element(By.CLASS_NAME, "opzioni_giustifica")
    note_type_span_list = note_type_div.find_elements(By.TAG_NAME, "span")
    note_type_radio = None

    # Find the radio input where the note type matches
    for type_span in note_type_span_list:
        current_input_el = type_span.find_element(By.TAG_NAME, "input")

        if note.notetype.value in type_span.text:
            note_type_radio = current_input_el
            break

    if note_type_radio == None:
        raise Exception("Note type radio input not found")

    CustomLogger.info(f"Selected note type '{note.type.name}'")
    note_type_radio.click()

    ########################
    #  SELECT NOTE CAUSAL  #
    ########################
    note_causal_input = driver.find_element(By.CLASS_NAME, f"check_{note.causal.value}")

    if note_causal_input == None:
        raise Exception("Note causal radio input not found")

    CustomLogger.info(f"Selected note causal '{note.causal.name}'")
    note_causal_input.click()

    WebDriverWait(driver, 1)

    ###########################
    #  FILL NOTE INFORMATIONS #
    ###########################
    if note.type == ClassevivaNoteType.ASSENZA:
        ##################################
        #  INSERT ASSENZA DATE INTERVAL  #
        ##################################
        date_splitted = note.notedate.split("-")
        if len(date_splitted) == 0 or len(date_splitted) > 2:
            raise Exception(f"Invalid date interval '{note.notedate}'")

        # If the date is single, use it for both start and end
        if len(date_splitted) == 1:
            start_day = date_splitted[0]
            end_day = date_splitted[0]
        else:
            start_day = date_splitted[0]
            end_day = date_splitted[1]

        start_day_input = driver.find_element(By.CLASS_NAME, "inizio_assenza")
        end_day_input = driver.find_element(By.CLASS_NAME, "fine_assenza")

        if not (start_day_input and end_day_input):
            raise Exception("Start or end date input not found")

        start_day_input.send_keys(start_day)
        end_day_input.send_keys(end_day)
        CustomLogger.info(f"Date interval set to '{start_day}-{end_day}'")

        ###############################
        #  INSERT ASSENZA MOTIVATION  #
        ###############################
        motivation_textarea = driver.find_element(By.CLASS_NAME, "motivazione_assenza")
        if motivation_textarea != None:
            motivation_textarea.send_keys(note.motivation)
        CustomLogger.info(f"Motivation set {note.motivation}")
    if (
        note.type == ClassevivaNoteType.ENTRATA
        or note.type == ClassevivaNoteType.USCITA
    ):
        ###############################
        #  INSERT ENTRATA/USCITA DAY  #
        ###############################
        day_input = driver.find_element(By.CLASS_NAME, "giorno_entrata_uscita")

        if not day_input:
            raise Exception("Day input not found")

        day_input.send_keys(note.notedate)
        CustomLogger.info(f"Day set to '{note.notedate}'")

        ################################
        #  INSERT ENTRATA/USCITA TIME  #
        ################################
        time_input = driver.find_element(By.CLASS_NAME, "ora_entrata_uscita")

        if not time_input:
            raise Exception("Time (hh:mm) input not found")

        time_input.send_keys(note.notetime)
        CustomLogger.info(f"Time set to '{note.notetime}'")

        ######################################
        #  INSERT ENTRATA/USCITA MOTIVATION  #
        ######################################
        motivation_textarea = driver.find_element(
            By.CLASS_NAME, "motivazione_entrata_uscita"
        )
        if motivation_textarea != None:
            motivation_textarea.send_keys(note.motivation)

    if note.type == ClassevivaNoteType.USCITA:
        companion_input = driver.find_element(By.CLASS_NAME, "accompagnatore")

        if companion_input != None:
            companion_input.send_keys(note.companion)
            CustomLogger.info(f"Companion set to '{note.companion}'")

    ##################
    #  CONFIRM NOTE  #
    ##################
    ui_buttons = driver.find_elements(By.CLASS_NAME, "ui-button")
    confirm_btn = None

    for btn in ui_buttons:
        if btn.text == "Conferma":
            confirm_btn = btn

    if confirm_btn == None:
        raise Exception("Confirm button not found")

    confirm_btn.click()

    WebDriverWait(driver, 10).until(
        lambda d: d.find_element(By.ID, "popupNuova"),
        message="Cannot confirm note, check your informations",
    )

    CustomLogger.info(f"Note confirmed successfully")
