from enum import Enum
from datetime import datetime


class ClassevivaNoteType(Enum):
    """Tipi di giustifica"""

    ASSENZA = "Assenza"
    ENTRATA = "Permesso di entrata"
    USCITA = "Permesso di uscita"


class ClassevivaNoteCausal(Enum):
    """Causali per giustifica"""

    SALUTE = "A"
    CERTIFICATO = "AC"
    FAMIGLIA = "B"
    ALTRO = "C"
    TRASPORTO = "D"
    SCIOPERO = "E"


class ClassevivaNote:
    """Classe per giustifica in Classeviva"""

    def __init__(
        self,
        notedate: str = datetime.now().strftime("%d/%m/%Y"),
        notetype: ClassevivaNoteType = ClassevivaNoteType.ASSENZA,
        causal: ClassevivaNoteCausal = ClassevivaNoteCausal.SALUTE,
        notetime: str = "",
        motivation: str = "",
        companion: str = "",
    ) -> None:
        # Check for valid notetype and causal
        if type(notetype) != ClassevivaNoteType:
            raise Exception("Invalid note type")
        if type(causal) != ClassevivaNoteCausal:
            raise Exception("Invalid causal")

        self._notedate = notedate
        self._notetype = notetype
        self._causal = causal
        self._notetime = notetime
        self._motivation = motivation or causal.name
        self._companion = companion

    @property
    def notedate(self) -> str:
        """Periodo della giustifica, (giorno singolo per entrata/uscita e intervallo per assenza)\n
        Formato: `gg/mm/aaaa`
        """
        return self._notedate

    @notedate.setter
    def notedate(self, value):
        self._notedate = value

    @property
    def notetype(self) -> ClassevivaNoteType:
        """Tipo della giustifica"""
        return self._notetype

    @notetype.setter
    def type(self, value):
        if value in ClassevivaNoteType:
            self._notetype = value
        else:
            raise ValueError(f"Invalid note type '{value}'")

    @property
    def causal(self) -> ClassevivaNoteCausal:
        """Causale della giustifica"""
        return self._causal

    @causal.setter
    def causal(self, value):
        if value in ClassevivaNoteCausal:
            self._causal = value
        else:
            raise ValueError(f"Invalid causal '{value}'")

    @property
    def notetime(self) -> str:
        """Orario nel formato `hh:mm` (per entrata/uscita)"""
        return self._notetime

    @notetime.setter
    def notetime(self, value):
        self._notetime = value

    @property
    def motivation(self) -> str:
        """Motivazione della giustifica"""
        return self._motivation

    @motivation.setter
    def motivation(self, value):
        if not value:
            self._motivation = self.causal.name
        else:
            self._motivation = value

    @property
    def companion(self) -> str:
        """Accompagnatore (per entrata/uscita)"""
        return self._companion

    @companion.setter
    def companion(self, value):
        self._companion = value
